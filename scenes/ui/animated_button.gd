class_name AnimatedButton
extends Control

var tween : Tween


signal button_pressed


func _ready() -> void:
	# We have to duplicate the texture and set our texture to the duplicate
	# Otherwise Godot is dumb and it references the same texture accross ALL instances of this scene
	# Not Exact the best memory wise, but you gotta do what you gotta do ig
	var our_texture = $TextureRect.texture.duplicate()
	$TextureRect.texture = our_texture
	tween = get_tree().create_tween()

func _on_button_mouse_entered() -> void:
	tween = get_tree().create_tween().set_trans(Tween.TRANS_QUART)
	#tween.tween_property($TextureRect.texture, "fill_from:x", 1, 0.5)
	#tween.tween_property($TextureRect.texture, "fill_from:y", 1, 0.5)
	tween.tween_property($TextureRect.texture, "fill_from", Vector2(1, 1), 0.5)


func _on_button_mouse_exited() -> void:
	tween = get_tree().create_tween().set_trans(Tween.TRANS_QUART)
	tween.tween_property($TextureRect.texture, "fill_from", Vector2(0, 0), 0.5)


func _on_button_pressed() -> void:
	button_pressed.emit()
