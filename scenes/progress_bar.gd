@tool
extends Node2D

@export var title : String = "Health":
	set(new_value):
		title = new_value
		update_text()


@export_enum("percent", "value", "both") var display_mode : String = "percent":
	set(new_value):
		display_mode = new_value
		update_text()
		

@export var min : float = 0:
	set(new_value):
		var tween = create_tween().set_parallel().set_ease(Tween.EASE_IN_OUT).set_trans(Tween.TRANS_CUBIC)
		tween.tween_property($Background/Blocker, "position", Vector2(0, -128 * remap(value, new_value, max, 0, 1)), 0.15 + abs(abs(new_value) - abs(min)) * 0.01)
		min = new_value
		update_text()
@export var max : float = 100:
	set(new_value):
		var tween = create_tween().set_parallel().set_ease(Tween.EASE_IN_OUT).set_trans(Tween.TRANS_CUBIC)
		tween.tween_property($Background/Blocker, "position", Vector2(0, -128 * remap(value, min, new_value, 0, 1)), 0.15 + abs(abs(new_value) - abs(max)) * 0.01)
		max = new_value
		update_text()
@export var value : float = 50:
	set(new_value):
		var old_value = value
		value = new_value
		update_text()
		var tween = create_tween().set_parallel().set_ease(Tween.EASE_IN_OUT).set_trans(Tween.TRANS_CUBIC)
		tween.tween_property($Background/Blocker, "position", Vector2(0, -128 * remap(new_value, min, max, 0, 1)), 0.15 + abs(abs(new_value) - abs(old_value)) * 0.01)

@export var main_color : Color = Color("d2aab9"):
	set(new_value):
		var tween = create_tween().set_parallel().set_ease(Tween.EASE_IN_OUT).set_trans(Tween.TRANS_CUBIC)
		tween.tween_property($Background/Stripes.material, "shader_parameter/color_stripe", new_value, 0.15)
		tween.tween_property($Label.label_settings, "font_color", new_value, 0.15)
		main_color = new_value
@export var sub_color : Color = Color("ff0000"):
	set(new_value):
		var tween = create_tween().set_parallel().set_ease(Tween.EASE_IN_OUT).set_trans(Tween.TRANS_CUBIC)
		tween.tween_property($Background/Stripes.material, "shader_parameter/color_gap", new_value, 0.15)
		sub_color = new_value

@export var speed : float = 0.1:
	set(new_value):
		var tween = create_tween().set_parallel().set_ease(Tween.EASE_IN_OUT).set_trans(Tween.TRANS_EXPO)
		tween.tween_property($Background/Stripes.material, "shader_parameter/speed", new_value, 0.15 + abs(abs(new_value) - abs(speed)) * 0.01)
		speed = new_value
@export var angle : float = 4.713:
	set(new_value):
		var tween = create_tween().set_parallel().set_ease(Tween.EASE_IN_OUT).set_trans(Tween.TRANS_EXPO)
		tween.tween_property($Background/Stripes.material, "shader_parameter/angle", new_value, 0.15 + abs(abs(new_value) - abs(angle)) * 0.01)
		angle = new_value
@export var divisions : float = 8:
	set(new_value):
		var tween = create_tween().set_parallel().set_ease(Tween.EASE_IN_OUT).set_trans(Tween.TRANS_EXPO)
		tween.tween_property($Background/Stripes.material, "shader_parameter/divisions", new_value, 0.15 + abs(abs(new_value) - abs(divisions)) * 0.01)
		divisions = new_value


func update_text():
	var tween = create_tween().set_parallel().set_ease(Tween.EASE_IN)
	var new_string = ""
	if display_mode == &"percent":
		new_string = title + " - " + str(int(snapped(remap(value, min, max, 0, 1) * 100, 0.01)) ) + "%"
	elif display_mode == &"value":
		new_string = title + " - " + str(value)
	else:
		new_string = title + " - " + str(value) + " (" + str(int(snapped(remap(value, min, max, 0, 1) * 100, 0.01))) + "%)"
	tween.tween_property($Label, "text", new_string, 0.1)

func _ready() -> void:
	var material = $Background/Stripes.material.duplicate()
	$Background/Stripes.material = material
	material = $Label.label_settings.duplicate()
	$Label.label_settings = material
	divisions = divisions
	angle = angle
	speed = speed
	sub_color = sub_color
	main_color = main_color
	value = value
	update_text()
	
	
	
	
