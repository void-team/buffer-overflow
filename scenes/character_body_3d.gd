extends CharacterBody3D


const SPEED = 7.0
const JUMP_VELOCITY = 4.5

const DASH_MULTIPLIER = 2

var dash : float = 1

var dash_energy : float = 3

func rotate_player(direction : Vector2, sensitivity : float = 0.0015, inverted := false):
	rotate_y(direction.x * sensitivity * (1 if not inverted else -1))
	$Head/Camera.rotation.x = clamp($Head/Camera.rotation.x + direction.y * sensitivity * (1 if not inverted else -1), deg_to_rad(-90), deg_to_rad(90))
func _ready() -> void:
	Input.mouse_mode = Input.MOUSE_MODE_CAPTURED


func _unhandled_input(event: InputEvent) -> void:
	if event.is_action("pause") && event.is_pressed():
		if !Input.mouse_mode == Input.MOUSE_MODE_VISIBLE:
			Input.mouse_mode = Input.MOUSE_MODE_VISIBLE
			$"../Pause".transition()
		else:
			Input.mouse_mode = Input.MOUSE_MODE_CAPTURED
			$"../Pause".transition(true)
		
		
	if event is InputEventMouseMotion:
		rotate_player(event.relative, 0.0015, true)
		return
	if event is InputEventMouseButton:
		Input.mouse_mode = Input.MOUSE_MODE_CAPTURED

func _physics_process(delta: float) -> void:
	# Add the gravity.
	if not is_on_floor():
		velocity += get_gravity() * delta

	# Handle jump.
	if Input.is_action_just_pressed("jump") and is_on_floor():
		velocity.y = JUMP_VELOCITY
	if Input.is_action_just_pressed("dash"):
		if dash_energy >= 1:
			dash = DASH_MULTIPLIER
			dash_energy -= 1
			$DashProgressBar.value = dash_energy
		
	# Get the input direction and handle the movement/deceleration.
	# As good practice, you should replace UI actions with custom gameplay actions.
	var input_dir := Input.get_vector("left", "right", "forwards", "backwards")
	var direction := (transform.basis * Vector3(input_dir.x, 0, input_dir.y)).normalized()
	if direction:
		velocity.x = direction.x * SPEED * dash
		velocity.z = direction.z * SPEED * dash
	else:
		velocity.x = move_toward(velocity.x, 0, SPEED)
		velocity.z = move_toward(velocity.z, 0, SPEED)

	move_and_slide()
	dash = clamp(dash - (delta * dash), 1, DASH_MULTIPLIER)
	
	if dash == 1 && dash_energy < 3:
		dash_energy = clamp(dash_energy + delta, 0, 3)
		$DashProgressBar.value = dash_energy
		
	
	input_dir = Input.get_vector("look_left", "look_right", "look_up", "look_down")
	rotate_player(input_dir, 0.07, true)
