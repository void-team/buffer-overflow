extends Node2D

@export var fade_in_sound : AudioStream = preload("res://audio/sfx/chime.wav")
@export var fade_out_sound : AudioStream = preload("res://audio/sfx/logoff.wav")
@export var click_sound : AudioStream = preload("res://audio/sfx/final/click.wav")

var items_available : Array[ItemSelectorItem]
var item_index : int = 0

var min_screen_y = 0
var max_screen_y = 0
var progress_increment = 0
var on_screen = false

var overflow_num : int = 3

var characters = 'abcdefghijklmnopqrstuvwxyz'


func _unhandled_input(event: InputEvent) -> void:
	if event.is_released():
		return
	if event.is_action("inventory"):
		if on_screen:
			transition_out()
		else:
			transition_in()
		on_screen = !on_screen
		return
	if on_screen:
		if event.is_action("ui_down"):
			item_index = clamp(item_index + 1, 0, items_available.size() - 1)
			$AudioStreamPlayer.stream = click_sound
			$AudioStreamPlayer.play()
			highlight(items_available[item_index], items_available[item_index - 1])
		elif event.is_action("ui_up"):
			item_index = clamp(item_index - 1, 0, items_available.size())
			$AudioStreamPlayer.stream = click_sound
			$AudioStreamPlayer.play()
			highlight(items_available[item_index], items_available[item_index + 1])



func highlight(new_highlight : ItemSelectorItem, old_highlight : ItemSelectorItem = null):
	new_highlight.highlighted = true
	var overflow_index = clamp(item_index, 0, items_available.size() - 4) - overflow_num
	var overflow_offset = -80 * (0 if overflow_index < 0 else overflow_index)
	
	var tween = get_tree().create_tween().set_parallel()
	tween.tween_property($Items, "position", Vector2(0, overflow_offset), 0.075)
	tween.tween_property(new_highlight, "position", new_highlight.position + Vector2(16, 0), 0.075)
	tween.tween_property($Selector, "position", new_highlight.position + Vector2(16, overflow_offset), 0.075)
	
	var progress_bar_material = $"Progress Bar/Polygon2D".material
	var text_settings = $"Progress Bar/Label".label_settings
	tween.tween_property($Selector/ColorRect, "color", new_highlight.primary_color, 0.075)
	tween.tween_property($"Progress Bar/ProgressNode/Polygon2D2", "color", new_highlight.primary_color, 0.075)
	tween.tween_property(progress_bar_material, "shader_parameter/color_gap", new_highlight.primary_color * 0.875, 0.075)	
	tween.tween_property(text_settings, "font_color", Color(new_highlight.primary_color, 0.8), 0.075)
	
	tween.tween_property($"Progress Bar/ProgressNode/Polygon2D3".material, "shader_parameter/color_stripe", new_highlight.primary_color * 1.25, 0.075)
	tween.tween_property($"Progress Bar/ProgressNode/Polygon2D4".material, "shader_parameter/color_stripe", new_highlight.primary_color * 1.25, 0.075)
	
	tween.tween_property($"Progress Bar/Label", "text", generate_word(characters, randi_range(1, 25)), 0.4)
	
	tween.tween_property($"Progress Bar/ProgressNode", "position", Vector2(0, item_index * progress_increment), 0.075)
	if old_highlight:
		old_highlight.highlighted = false
		tween.tween_property(old_highlight, "position", Vector2(0, old_highlight.position.y), 0.075)

func find_min_max():
	var min_found = false
	$TESTER.position.y = 40
	while !min_found:
		$TESTER.position.y -= 80
		if ($TESTER.global_position.x < 8 or 
			$TESTER.global_position.y < 16 or
			$TESTER.global_position.y > get_viewport().size.y - 8):
			min_screen_y = $TESTER.position.y + 80
			min_found = true
	var max_found = false
	$TESTER.position.y = 40
	while !max_found:
		$TESTER.position.y += 80
		if ($TESTER.global_position.x > get_viewport().size.x - 8 or 
			$TESTER.global_position.y > get_viewport().size.y - 16):
			max_screen_y = $TESTER.position.y - 80
			max_found = true

func transition_in():
	$AudioStreamPlayer.stream = fade_in_sound
	$AudioStreamPlayer.play()
	var tween = get_tree().create_tween().set_parallel().set_ease(Tween.EASE_OUT_IN)
	var iterator = 0
	
	tween.tween_property($TextureRect3.texture, "fill_to", Vector2(0.5, 0), 0.2)
	tween.tween_property($TextureRect3.texture, "fill_from", Vector2(0.5, 0), 0.4)
	tween.tween_property($"Progress Bar", "position", Vector2(96, min_screen_y), 0.4)
	tween.tween_property($Selector/ColorRect, "color", items_available[0].primary_color, 0.2)
	for i in items_available:
		tween.tween_property(i, "position", Vector2(0, min_screen_y + (80 * iterator)), clamp(0.2 + (iterator * 0.15), 0, 1.5))
		iterator += 1
	tween.tween_property($Selector, "position", Vector2(0, min_screen_y), 0.4)
	await tween.finished
	highlight(items_available[item_index])
	
func transition_out():
	$AudioStreamPlayer.stream = fade_out_sound
	$AudioStreamPlayer.play()
	var tween = get_tree().create_tween().set_parallel().set_ease(Tween.EASE_OUT_IN)
	var iterator = 0
	tween.tween_property($TextureRect3.texture, "fill_to", Vector2(0.5, 1), 0.2)
	tween.tween_property($TextureRect3.texture, "fill_from", Vector2(0.5, 1), 0.4)
	
	tween.tween_property($Selector/ColorRect, "color", items_available[0].primary_color, 0.2)
	for i in items_available:
		tween.tween_property(i, "position", Vector2(-120, get_viewport().size.y + 60), 0.25)
		iterator += 1
	tween.tween_property($Selector, "position", Vector2(-120, get_viewport().size.y + 60), 0.25)
	tween.tween_property($"Progress Bar", "position", Vector2(-120, get_viewport().size.y + 60), 0.25)
	await tween.finished
	item_index = 0
	highlight(items_available[0])


func _ready() -> void:
	
	find_min_max()
	for i in $Items.get_children():
		i.position = Vector2(-120, get_viewport().size.y + 60)
		if i is ItemSelectorItem:
			items_available.append(i)
	# I don't know why we have to subtract the total size from one
	# I don't understand it, but it works, so I'm not gonna bother it
	progress_increment = (abs(min_screen_y) + abs(max_screen_y) ) / (items_available.size() - 1)
	
	$"Progress Bar".position = Vector2(96, min_screen_y)
	var polygon = PackedVector2Array()
	polygon.append(Vector2(-16, -24))
	polygon.append(Vector2(16, -40))
	polygon.append(Vector2(16, max_screen_y + abs(min_screen_y) + 24))
	polygon.append(Vector2(-16, max_screen_y + abs(min_screen_y) + 40))
	$"Progress Bar/Polygon2D".polygon = polygon
	$"Progress Bar".position = Vector2(-120, get_viewport().size.y + 60)
	overflow_num = int(((abs(min_screen_y) + abs(max_screen_y)) / 80) / 2)
	
	highlight(items_available[item_index], items_available[item_index + 1])
	
func generate_word(chars, length):
	var word: String
	var n_char = len(chars)
	for i in range(length):
		word += chars[randi()% n_char]
	return word
