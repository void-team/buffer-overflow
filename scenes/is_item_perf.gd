extends VisibleOnScreenNotifier2D




func _on_screen_entered() -> void:
	$"../SubViewportContainer/SubViewport/Node3D/Camera3D".set_cull_mask_value(20, true)
	$"../SubViewportContainer/SubViewport/Node3D/Camera3D".make_current()


func _on_screen_exited() -> void:
	$"../SubViewportContainer/SubViewport/Node3D/Camera3D".set_cull_mask_value(20, false)
	$"../SubViewportContainer/SubViewport/Node3D/Camera3D".clear_current()

func _ready() -> void:
	$"../SubViewportContainer/SubViewport/Node3D/Camera3D".set_cull_mask_value(20, false)
	$"../SubViewportContainer/SubViewport/Node3D/Camera3D".clear_current()
