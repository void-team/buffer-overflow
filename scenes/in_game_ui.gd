extends Control


func _ready() -> void:
	position = Vector2(0, 720)
	$"../TextureRect".texture.fill_from = Vector2(1, 0)
	$"../TextureRect".texture.fill_to = Vector2(1, 0)
	
	$"../TextureRect2".texture.fill_from = Vector2(0.5, 1)
	$"../TextureRect2".texture.fill_to = Vector2(0.5, 1)
func transition(out := false):
	var tween := get_tree().create_tween().set_parallel()
	if !out:
		
		tween.tween_property($"../TextureRect".texture, "fill_to", Vector2(0, 1), 0.15)
		tween.tween_property($"../TextureRect".texture, "fill_from", Vector2(0, 1), 0.3)
		tween.set_parallel(false)
		tween.tween_property(self, "position", Vector2(0, 0), 0.3)
		tween.set_parallel(true)
		tween.tween_property($"../TextureRect2".texture, "fill_to", Vector2(0.5, 0), 0.15)
		tween.tween_property($"../TextureRect2".texture, "fill_from", Vector2(0.5, 0), 0.3)
		
	else:
		
		tween.tween_property($"../TextureRect".texture, "fill_to", Vector2(1, 0), 0.15)
		tween.tween_property($"../TextureRect".texture, "fill_from", Vector2(1, 0), 0.3)
		tween.set_parallel(false)
		tween.tween_property($"../TextureRect2".texture, "fill_to", Vector2(0.5, 1), 0.15)
		tween.set_parallel(true)
		tween.tween_property($"../TextureRect2".texture, "fill_from", Vector2(0.5, 1), 0.3)
		tween.tween_property(self, "position", Vector2(0, 720), 0.3)
		
