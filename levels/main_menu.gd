extends Control

var tween : Tween
var simultaneous_scene = preload("res://levels/transition.tscn").instantiate()

func _add_a_scene_manually():
	# This is like autoloading the scene, only
	# it happens after already loading the main scene.
	get_tree().root.call_deferred("add_child", simultaneous_scene)

func _on_enter_game_button_button_pressed() -> void:
	simultaneous_scene.next_scene = "res://levels/testing.tscn"
	tween = get_tree().create_tween().set_trans(Tween.TRANS_SINE)
	tween.tween_property($TextureRect.texture, "fill_from", Vector2(1, 1), (0.3*0.667))
	tween.tween_property($TextureRect2.texture, "fill_from", Vector2(1, 0), (0.3*0.667))
	tween.tween_property($TextureRect4.texture, "fill_from", Vector2(0, 1), (0.3*0.667))
	tween.set_parallel(false)
	tween.tween_property($TextureRect3.texture, "fill_from", Vector2(0, 0.5), (0.3*0.667))
	await tween.finished
	_add_a_scene_manually()
	call_deferred("queue_free")
	
