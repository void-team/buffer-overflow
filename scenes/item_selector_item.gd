class_name ItemSelectorItem
extends Node2D

@export var primary_color : Color
@export var secondary_color : Color

@onready var post_process = $SubViewportContainer/SubViewport/Node3D/Camera3D/MeshInstance3D

@export var displayed_mesh : Mesh

@export var display_rotation = Vector3(0, 0, 0)

@export var display_scale = Vector3(1, 1, 1)

@export var highlighted = false:
	set(value):
		highlighted = value
		if !highlighted:
			$SubViewportContainer/SubViewport/Node3D/MeshInstance3D.position.y = 0

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	$SubViewportContainer/SubViewport/Node3D/MeshInstance3D.mesh = displayed_mesh
	$SubViewportContainer/SubViewport/Node3D/MeshInstance3D.rotation = display_rotation
	$SubViewportContainer/SubViewport/Node3D/MeshInstance3D.scale = display_scale
	$SubViewportContainer/SubViewport/Node3D.position = Vector3(randf_range(100, 200) * randf_range(1, 12), randf_range(-100, -200) * randf_range(1, 8), randf_range(100, 200) * randf_range(1, 8))
	var mat = ShaderMaterial.new()
	mat.shader = preload("res://materials/shaders/color_quantization.gdshader")
	mat.set_shader_parameter("pallate", [Color(0,0,0), primary_color, primary_color * 0.7, primary_color * 0.5, secondary_color, secondary_color * 0.7, secondary_color * 0.5])
	post_process.set_surface_override_material(0, mat)

func _process(delta: float) -> void:
	$SubViewportContainer/SubViewport/Node3D/MeshInstance3D.rotate_y(delta * 0.75)
	$SubViewportContainer/SubViewport/Node3D/MeshInstance3D.rotate_z(delta * 0.1)
	
	if highlighted:
		$SubViewportContainer/SubViewport/Node3D/MeshInstance3D.position.y = sin(Time.get_ticks_msec() / 1000.0) * 0.1
